module.exports = {
  target: "static",
  /*
   ** Headers of the page
   */
  head: {
    title: "hdiseguro.mx",
    htmlAttrs: { lang: "es-MX" },
    meta: [
      { charset: "utf-8" },
      { hid: "robots", name: "robots", content: "index, follow" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "HDI" },
    ],
    link: [{ rel: "icon", type: "image/png", href: "favicon.png" }],
  },
  /*
   ** Customize the progress bar color
   */
  loading: { color: "#3bd600" },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** Run ESLint on save
     */
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          exclude: /(node_modules)/,
        });
      }
    },
  },
  // include css
  css: ["static/css/bootstrap.min.css", "static/css/styles.min.css"],
  plugins: [
    { src: "~/plugins/filters.js", ssr: false },
    { src: "~/plugins/apm-rum.js", ssr: false },
  ],
  modules: ["@nuxtjs/axios"],
  // gtm:{ id: 'GTM-NV6V6HB' },
  env: {
    tokenData: "mHf/0x8xqWmYlrjaRWECOzmkksnuNDZv1fBvMLjpI2g=", // TOKEN DATA
    catalogo: "https://dev.ws-hdiseguro.com", // CATALOGO
    coreBranding: "https://dev.core-brandingservice.com", // CORE
    coreSale: "https://dev.core-persistance-service.com", // CORE
    motorCobro: "https://p.hdiseguro.mx/seguros-de-auto/compra/cliente", // MOTOR DE COBRO
    sitio: "https://p.hdiseguro.mx/seguros-de-auto", // SITIO
    hubspot:"https://core-hubspot-dev.mark-43.net/deals/landing",
    urlValidaciones: "https://core-blacklist-service.com/rest", // VALIDACIONES
    Environment: "DEVELOP",
  },
  router: {
    base: "/seguros-de-auto/",
  },
  render: {
    http2: { push: true },
    resourceHints: true,
    compressor: { threshold: 9 },
  },
};
