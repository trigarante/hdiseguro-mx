import axios from 'axios'


const cotizacionService = {}

cotizacionService.search = function (peticion, accessToken) {

  return axios({
    method: "post",
    headers: {'Authorization': 'Bearer '+accessToken},
    url: process.env.coreSale + '/v2/hdi/quotation',
    data: JSON.parse(peticion)
  })
}
export default cotizacionService


