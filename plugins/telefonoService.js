 
import axios from "axios"
 
async function telefonoService (idDiffusionMedium) {
    return axios({
        method: "get",
        url: process.env.coreSale + '/v1/page/diffusion-medium/phone?idDiffusionMedium='+idDiffusionMedium,    
    })
}
 
export default telefonoService;