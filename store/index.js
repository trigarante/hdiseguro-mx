import Vuex from "vuex";
import getTokenService from "~/plugins/getToken";

const createStore = () => {
  const tokenData = process.env.tokenData;
  return new Vuex.Store({
    state: {
      idEndPoint: "",
      status: "",
      message: "",
      cargandocotizacion: false,
      msj: false,
      msjEje: false,
      sin_token: false,
      tiempo_minimo: 1.3,
      config: {
        time: '',
        reload: true,
        habilitarBtnEmision: false,
        habilitarBtnInteraccion: false,
        aseguradora: "",
        cotizacion: true,
        emision: true,
        interaccion: true,
        btnTop: '/',
        descuento: 0,
        telefonoAS: "47495127",
        grupoCallback: "",
        from: "",
        idPagina: 0,
        idMedioDifusion: 0,
        idMedioDifusionInteraccion: 0,
        idMedioDifusionEcommerce: 0,
        idSubRamo: "",
        id_cotizacion_a: "0",
        dominioCorreo: "",
        img: "",
        pathImg: "/seguros-de-auto/img/",
        MostrarModal: false,
        alert: true,
        asgNombre: "HDI",
        promo: "",
        desc: "",
        msi: "",
        dominio: "hdiseguro.mx",
        urlGracias: "",
        accessToken: "",
        ipCliente: "",
        cotizacionAli: "",
        textoSMS:
          "Gracias por cotizar en hdiseguros.mx, en un momento uno de nuestros ejecutivos se pondrá en contacto contigo.",
        desc: "",
        msi: "",
        promoLabel: "",
        promoImg: "",
        promoSpecial: false,
        extraMsg: "",
        logoAseguradora: "https://prod-mx.sfo3.cdn.digitaloceanspaces.com/prod_asmx/ahorraseguros.mx/wp-content/themes/nexoslab/assets/logotipos/hdi/logo.svg",
        logoagenteAutorizado: 'https://prod-mx.sfo3.cdn.digitaloceanspaces.com/prod_asmx/ahorraseguros.mx/wp-content/themes/nexoslab/assets/logotipos/operado-por-ahorraguros.svg'
      },
      ejecutivo: {
        nombre: "",
        correo: "",
        id: 0,
      },
      formData: {
        aseguradora: "",
        marca: "",
        modelo: "",
        descripcion: "",
        detalle: "",
        clave: "",
        cp: "",
        nombre: "",
        telefono: "",
        gclid_field: "",
        correo: "",
        edad: "",
        fechaNacimiento: "",
        genero: "",
        emailValid: "",
        telefonoValid: "",
        codigoPostalValid: "",
        dominioCorreo: "",
        urlOrigen: "",
        utmSource:'',
        utmMedium:'',
        utmCampaign:'',
        utmId:'',
        idHubspot:'',
        idLogData:'',
      },
      solicitud: {},
      cotizacion: {},
      cotizacionesAmplia: [],
      cotizacionesLimitada: [],
    },
    actions: {
      getToken(state) {
        return new Promise((resolve, reject) => {
          getTokenService.search(tokenData).then(
            (resp) => {
              if (typeof resp.data == "undefined") {
                this.state.config.accessToken = resp.accessToken;
              } else if (typeof resp.data.accessToken != "undefined") {
                this.state.config.accessToken = resp.data.accessToken;
              }
              localStorage.setItem("authToken", this.state.config.accessToken);
              resp = resp.data;
              resolve(resp);
            },
            (error) => {
              reject(error);
            }
          );
        });
      },
    },
    mutations: {
      validarTokenCore: function (state) {
        try {
          if (process.browser) {
            if (localStorage.getItem("authToken") === null) {
              state.sin_token = true;
              console.log("NO HAY TOKEN...");
            } else {
              console.log("VALIDANDO TOKEN...");
              state.config.accessToken = localStorage.getItem("authToken");
              var tokenSplit = state.config.accessToken.split(".");
              var decodeBytes = atob(tokenSplit[1]);
              var parsead = JSON.parse(decodeBytes);
              var fechaUnix = parsead.exp;
              /*
               * Fecha formateada de unix a fecha normal
               * */
              var expiracion = new Date(fechaUnix * 1000);
              var hoy = new Date(); //fecha actual
              /*
               * Se obtiene el tiempo transcurrido en milisegundos
               * */
              var tiempoTranscurrido = expiracion - hoy;
              /*
               * Se obtienen las horas de diferencia a partir de la conversión de los
               * milisegundos a horas.
               * */
              var horasDiferencia =
                Math.round(
                  (tiempoTranscurrido / 3600000 + Number.EPSILON) * 100
                ) / 100;

              if (hoy > expiracion || horasDiferencia < state.tiempo_minimo) {
                state.sin_token = "expirado";
              }
            }
          }
        } catch (error2) {
          console.log(error2);
        }
      },
    },
  });
};
export default createStore;
